# JPiwigoSync

A simple application to sync my photos with : 
* Remote piwigo
* Openstack Object Storage
* MySQL database

## Usage

Create a **jpiwigosync.properties** file :

    input_directory=/tmp/photos/
    jdbc_constring=jdbc:mysql://<mysql_host>/<mysql_database>
    jdbc_username=<username>
    jdbc_password=<password>

    os_auth_endpoint=https://auth.cloud.ovh.net/v2.0/
    os_region=SBG1
    os_username=<os username>
    os_password=<os password>
    os_tenant_name=<tenant name>
    os_container_name=photos_hd

    upload_galleries_directory=/var/www/photos/galleries
    upload_host=photos.example.com
    upload_user=debian
    upload_key=/home/<user>/.ssh/id_rsa%   


