package info.kapable.utils.jpiwigosync.model;

import junit.framework.TestCase;
import info.kapable.utils.jpiwigosync.model.*;

public class FileMetadataTest extends TestCase {
	/**
	 * Method to test return of getfileextention
	 */
	public void testGetFileExtention() {
		FileMetadata f = new FileMetadata();
		f.setFilename("test.JPG");
		String ext = f.getFileExtention();
		assertTrue(ext.contentEquals("jpg"));
		f.setFilename("test.MOV");
		assertTrue(f.getFileExtention().contentEquals("mov"));
	}
}
