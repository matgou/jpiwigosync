package info.kapable.utils.jpiwigosync.processor;

import java.io.File;

import org.springframework.batch.item.ItemProcessor;

import info.kapable.utils.jpiwigosync.model.FileMetadata;
import info.kapable.utils.jpiwigosync.service.ImageScpUploadService;

public class GalleriesUploadProcessor implements ItemProcessor<FileMetadata, FileMetadata>
{
	private ImageScpUploadService uploader;
	private String inputDirecotry;
	
	@Override
	public FileMetadata process(FileMetadata file) throws Exception {
		uploader.upload(new File(file.getResizedFilePath()));
		file.setUploaded(true);
		
		return file;
	}
	
	public ImageScpUploadService getUploader() {
		return uploader;
	}
	public void setUploader(ImageScpUploadService uploader) {
		this.uploader = uploader;
	}
	public String getInputDirecotry() {
		return inputDirecotry;
	}
	public void setInputDirecotry(String inputDirecotry) {
		this.inputDirecotry = inputDirecotry;
	}
}
