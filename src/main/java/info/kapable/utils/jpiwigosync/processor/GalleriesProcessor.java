package info.kapable.utils.jpiwigosync.processor;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import info.kapable.utils.jpiwigosync.model.FileMetadata;
import info.kapable.utils.jpiwigosync.service.ImageResizeService;
import info.kapable.utils.jpiwigosync.service.AbstractResizeService;
import info.kapable.utils.jpiwigosync.service.MovieResizeService;

public class GalleriesProcessor implements ItemProcessor<FileMetadata, FileMetadata>
{
	// The logger
	private static Logger logger = LoggerFactory.getLogger(GalleriesProcessor.class);

    // reference the input directory for work with relative path
    private String inputDirecotry;
	
    // ResizerService to 
	private ImageResizeService imageResizer;
	private MovieResizeService movieResizer;

	@Override
	public FileMetadata process(FileMetadata file) throws Exception {
		logger.info("GalleriesProcessor => processing : " + file.getFilename());
		AbstractResizeService resizer = this.getResizerFromFilename(file.getFilename());

		File fileObject = new File(this.inputDirecotry + "/" + file.getFilename());
		
		File resizedFile = resizer.buildResizeImage(fileObject);
		file.setResizedFilePath(resizedFile.getAbsolutePath());
		
		return file;
	}

	private AbstractResizeService getResizerFromFilename(String filename) {
		if(filename.toLowerCase().endsWith(".mov")) {
			return this.getMovieResizer();
		}
		return this.getImageResizer();
	}
	
	public MovieResizeService getMovieResizer() {
		return movieResizer;
	}
	
	public void setMovieResizer(MovieResizeService movieResizer) {
		this.movieResizer = movieResizer;
	}
	
	public ImageResizeService getImageResizer() {
		return imageResizer;
	}

	public void setImageResizer(ImageResizeService imageResizer) {
		this.imageResizer = imageResizer;
	}

	public String getInputDirecotry() {
		return inputDirecotry;
	}

	public void setInputDirecotry(String inputDirecotry) {
		this.inputDirecotry = inputDirecotry;
	}

}
