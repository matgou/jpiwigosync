package info.kapable.utils.jpiwigosync.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import info.kapable.utils.jpiwigosync.model.PiwigoCategories;
import info.kapable.utils.jpiwigosync.model.PiwigoImage;
import info.kapable.utils.jpiwigosync.service.PiwigoApiService;

public class CreateVirtualGalleriesProcessor  implements ItemProcessor<PiwigoCategories, PiwigoCategories> {

	// The logger
    private static Logger logger = LoggerFactory.getLogger(ApiGalleriesProcessor.class);
    
    PiwigoApiService apiService;
    
	@Override
	public PiwigoCategories process(PiwigoCategories categorie) throws Exception {
		logger.info("Create virtual categories");
		
		int virtualCategorieId = this.apiService.createCategorie(categorie.getName(),categorie.getVirtualParentId());
		categorie.setVirtualId(virtualCategorieId);
		
		if(categorie.getImages() != null) {
			for(PiwigoImage i: categorie.getImages()) {
				logger.info(" add image to virtual categories " + i.toString());
				this.apiService.addImageToCategorie(i, virtualCategorieId);
			}
		}
		
		return categorie;

	}

	public PiwigoApiService getApiService() {
		return apiService;
	}

	public void setApiService(PiwigoApiService apiService) {
		this.apiService = apiService;
	}

}
