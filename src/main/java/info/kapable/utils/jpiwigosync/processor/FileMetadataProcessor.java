package info.kapable.utils.jpiwigosync.processor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import info.kapable.utils.jpiwigosync.model.FileMetadata;
import info.kapable.utils.jpiwigosync.service.FileMetadataRepositoryService;

public class FileMetadataProcessor implements ItemProcessor<FileMetadata, FileMetadata> {

	// The logger
    private static Logger logger = LoggerFactory.getLogger(FileMetadataProcessor.class);
    
    // reference to database for search if filemetadata exist
    private FileMetadataRepositoryService fmRepository;

    // reference the input directory for work with relative path
    private String inputDirecotry;
    
	@Override
	public FileMetadata process(FileMetadata item) throws Exception {
		FileMetadata mt;
		
		File f = new File(item.getFilename());
		long lastModified = f.lastModified();
		
		String filename = f.getAbsolutePath().substring(this.inputDirecotry.length());
		
		mt = this.fmRepository.select(filename, lastModified);
		if(mt == null) {
			return null;
		}
		
		if(mt.isExistInDb()) {
			logger.info("Processing..." + item + " ( as update )");
		} else {
			logger.info("Processing..." + item + " ( as new )");
		}
		try (InputStream is = Files.newInputStream(Paths.get(item.getFilename()))) {
			String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
			mt.setMd5(md5);
		} catch (IOException e) {
			logger.error("Error while read file", e);
			throw e;
		}
		return mt;
	}

	public String getInputDirecotry() {
		return inputDirecotry;
	}

	public void setInputDirecotry(String inputDirecotry) {
		this.inputDirecotry = inputDirecotry;
	}

	public FileMetadataRepositoryService getFmRepository() {
		return fmRepository;
	}

	public void setFmRepository(FileMetadataRepositoryService fmRepository) {
		this.fmRepository = fmRepository;
	}
}