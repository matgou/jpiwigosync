package info.kapable.utils.jpiwigosync.processor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import info.kapable.utils.jpiwigosync.model.PiwigoCategories;
import info.kapable.utils.jpiwigosync.model.PiwigoImage;
import info.kapable.utils.jpiwigosync.service.PiwigoApiService;

public class ApiGalleriesProcessor implements ItemProcessor<PiwigoCategories, PiwigoCategories> {

	// The logger
    private static Logger logger = LoggerFactory.getLogger(ApiGalleriesProcessor.class);
    
    PiwigoApiService apiService;
    DriverManagerDataSource dataSource;
    
	@Override
	public PiwigoCategories process(PiwigoCategories categorie) throws Exception {
		logger.info(categorie.getId() + " => " + categorie.getName());
		PiwigoCategories newCategorie = apiService.getCategorie(categorie.getId());
		newCategorie.setImages((info.kapable.utils.jpiwigosync.model.PiwigoImage[]) this.listImage(categorie));
		int virtualParentId = getVirtualParent(newCategorie.getParentId());

		if(virtualParentId > 0) {
			newCategorie.setVirtualParentId(virtualParentId);
			

		    if(newCategorie.getImages() != null) {
		    	logger.debug(" => NOMBRE d'image : " + newCategorie.getImages().length);
		    } else {
		    	logger.info(" => PAS d'image dans la categorie");
		    }
			return newCategorie;
		}
		logger.info(" => Parent introuvable, virtualisation la prochaine fois");
		return null;
	}
	
	private PiwigoImage[] listImage(PiwigoCategories categorie) throws SQLException {
		List<PiwigoImage> images = new ArrayList<PiwigoImage>();
		
		Connection connection = dataSource.getConnection();

		String sql = "select image_id from piwigo_image_category where piwigo_image_category.category_id = " + categorie.getId();
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		
		while (resultSet.next()) {
            // store virtualParentId
			PiwigoImage image = new PiwigoImage();
			String id = resultSet.getString("image_id");
			image.setId(id);
			images.add(image);
		}
	    resultSet.close();
		connection.close();
		
		
		PiwigoImage[] array = new PiwigoImage [ images.size() ];
		images.toArray(array);
		return array;
	}

	private int getVirtualParent(int parentId) throws SQLException {
		Connection connection = dataSource.getConnection();
        // let's assume table name is student
		String sql = "select id_categorie_virtual from piwigo_virtualized_categories where id_categorie_physical=" + parentId;
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		int virtualParentId = -1;
		
		while (resultSet.next()) {
            // store virtualParentId
			virtualParentId = resultSet.getInt("id_categorie_virtual");
		}
	    resultSet.close();
		connection.close();
		
		return virtualParentId;
	}

	public PiwigoApiService getApiService() {
		return apiService;
	}

	public void setApiService(PiwigoApiService apiService) {
		this.apiService = apiService;
	}

	public DriverManagerDataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DriverManagerDataSource dataSource) {
		this.dataSource = dataSource;
	}
}
