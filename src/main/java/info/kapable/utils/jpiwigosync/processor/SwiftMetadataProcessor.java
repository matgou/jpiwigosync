package info.kapable.utils.jpiwigosync.processor;

import java.io.File;
import java.security.SecureRandom;

import org.springframework.batch.item.ItemProcessor;

import info.kapable.utils.jpiwigosync.model.FileMetadata;
import info.kapable.utils.jpiwigosync.service.EncodingService;
import info.kapable.utils.jpiwigosync.service.OpenstackSwiftService;

public class SwiftMetadataProcessor implements ItemProcessor<FileMetadata, FileMetadata> {

	/**
	 * Service for normalize filename
	 */
	private EncodingService encoding;
	
	/**
	 * Service for use random
	 */
	private SecureRandom random;
	
	/**
	 * Service for swift usage
	 */
	private OpenstackSwiftService swift;

    // reference the input directory for work with relative path
    private String inputDirecotry;
	
	@Override
	public FileMetadata process(FileMetadata fileMetadata) throws Exception {
		// If file is not register in db generate metadata
		
		if(!fileMetadata.isExistInDb()) {
			fileMetadata.setHash(this.newHash()); // new random hash
			fileMetadata.setSwiftFilename(encoding.normalize(fileMetadata.getFilename()) + "-" + fileMetadata.getHash() + "." + fileMetadata.getFileExtention());
			
			swift.create(new File(inputDirecotry + "/" + fileMetadata.getFilename()), fileMetadata);
		}else {
			swift.update(new File(inputDirecotry + "/" + fileMetadata.getFilename()), fileMetadata);
		}
		
		return fileMetadata;
	}

	private String newHash() {
		return org.apache.commons.codec.digest.DigestUtils.md5Hex(Random());
	}

	private byte[] Random() {
		byte[] bytes = new byte[128];
		random.nextBytes(bytes );
		return bytes;
	}

	public EncodingService getEncoding() {
		return encoding;
	}

	public void setEncoding(EncodingService encoding) {
		this.encoding = encoding;
	}

	public SecureRandom getRandom() {
		return random;
	}

	public void setRandom(SecureRandom random) {
		this.random = random;
	}

	public OpenstackSwiftService getSwift() {
		return swift;
	}

	public void setSwift(OpenstackSwiftService swift) {
		this.swift = swift;
	}

	public String getInputDirecotry() {
		return inputDirecotry;
	}

	public void setInputDirecotry(String inputDirecotry) {
		this.inputDirecotry = inputDirecotry;
	}
}
