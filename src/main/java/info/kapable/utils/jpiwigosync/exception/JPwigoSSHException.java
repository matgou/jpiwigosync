package info.kapable.utils.jpiwigosync.exception;

/**
 * Exception while send file to remote piwigo server
 * @author Mathieu GOULIN <mathieu.goulin@gadz.org>
 */
public class JPwigoSSHException extends Exception {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -5399153223269160965L;

	/**
	 * Initialize a new Exception
	 * @param string text of the exception
	 */
	public JPwigoSSHException(String string) {
		super(string);
	}

}
