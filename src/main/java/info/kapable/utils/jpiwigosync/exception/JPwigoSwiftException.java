package info.kapable.utils.jpiwigosync.exception;

public class JPwigoSwiftException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7915973158728386204L;

	public JPwigoSwiftException(String string) {
		super(string);
	}

}
