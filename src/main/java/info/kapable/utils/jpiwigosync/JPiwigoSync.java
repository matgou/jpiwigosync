package info.kapable.utils.jpiwigosync;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Main class of JPiwigoSync
 * 
 * JPiwigoSync is a batch to sync a photo folder with piwigo and Openstack Swift
 * @author matgou
 *
 */
public class JPiwigoSync {
	
	// The logger
    private static Logger logger = LoggerFactory.getLogger(JPiwigoSync.class);
    
    /**
     * Main method, entrypoint
     * @param args
     */
	public static void main(String[] args) {
		logger.info("JPiwigoSync");

		String[] springConfig  = 
			{	
				args[0] 
			};
			
		@SuppressWarnings("resource")
		ApplicationContext context = 
				new ClassPathXmlApplicationContext(springConfig);
			
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job job = (Job) context.getBean("MainJob");

		try {
			JobExecution execution = jobLauncher.run(job, new JobParameters());
			logger.info("Exit Status : " + execution.getStatus());
		} catch (Exception e) {
			logger.error("Error in job execution", e);
		}

		logger.info("Done");
		System.exit(0);
	}
}
