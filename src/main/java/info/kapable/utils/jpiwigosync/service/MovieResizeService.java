package info.kapable.utils.jpiwigosync.service;

import java.io.File;
import java.io.IOException;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MovieResizeService extends AbstractResizeService {
	// The logger
	private static Logger logger = LoggerFactory.getLogger(MovieResizeService.class);

	private String gallerieDirecotry;
	private String inputDirecotry;
	private EncodingService encoding;
	private String gallerieExtention = "mp4";
	
	@Override
	public File buildResizeImage(File largeImage) throws IOException, ImageReadException, ImageWriteException {
		String unFormatedPath = largeImage.getParentFile().getPath().substring(this.inputDirecotry.length());
		String formatedPath = encoding.normalize(unFormatedPath);
		String formatedName = encoding.normalize(largeImage.getName());

		String newName = formatedName.substring(0, formatedName.lastIndexOf(".") + 1) + gallerieExtention;
		File output = new File(this.gallerieDirecotry + "/" + formatedPath + "/" + newName );
		
		logger.info("Create dir : " + output.getParentFile().getAbsolutePath());
		output.getParentFile().mkdirs();
		
		logger.info("Running : ffmpeg -i " + largeImage.getAbsolutePath() + " " + output.getAbsolutePath());
		// ffmpeg -i /home/user/Desktop/sample.mov /home/user/Desktop/sample.mp4
		Process process = Runtime.getRuntime()
			      .exec(String.format("ffmpeg -y -i \"%s\" \"%s\"", largeImage.getAbsolutePath(), output.getAbsolutePath()));
		
		int exitCode;
		try {
			exitCode = process.waitFor();
			assert exitCode == 0;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Error when running ffmpeg", e);
			throw new ImageWriteException("Error when running ffmpeg");
		}
		
		return output;
	}

	public String getGallerieDirecotry() {
		return gallerieDirecotry;
	}

	public void setGallerieDirecotry(String gallerieDirecotry) {
		this.gallerieDirecotry = gallerieDirecotry;
	}

	public String getInputDirecotry() {
		return inputDirecotry;
	}

	public void setInputDirecotry(String inputDirecotry) {
		this.inputDirecotry = inputDirecotry;
	}

	public EncodingService getEncoding() {
		return encoding;
	}

	public void setEncoding(EncodingService encoding) {
		this.encoding = encoding;
	}
}
