package info.kapable.utils.jpiwigosync.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.coobird.thumbnailator.Thumbnails;

public class ImageResizeService extends AbstractResizeService {
	// The logger
	private static Logger logger = LoggerFactory.getLogger(ImageResizeService.class);
	
	private int width;
	private String gallerieDirecotry;
	private String inputDirecotry;
	private EncodingService encoding;

	/**
	 * 
	 * @param largeImage
	 * @throws IOException 
	 * @throws ImageReadException 
	 * @throws ImageWriteException 
	 */
	public File buildResizeImage(File largeImage) throws IOException, ImageReadException, ImageWriteException {
		String unFormatedPath = largeImage.getParentFile().getPath().substring(this.inputDirecotry.length());
		String formatedPath = encoding.normalize(unFormatedPath);
		String formatedName = encoding.normalize(largeImage.getName());
		
		File temp_output = new File(this.gallerieDirecotry + "/" + formatedPath + "/" + formatedName + ".work");
		File output = new File(this.gallerieDirecotry + "/" + formatedPath + "/" + formatedName );
        
		logger.info("Create dir : " + output.getParentFile().getAbsolutePath());
		output.getParentFile().mkdirs();
        
		logger.info("Convert Image : " + largeImage.getName());		
		OutputStream temp_os = new FileOutputStream(temp_output);
    	Thumbnails.of(largeImage)
    		.width(this.getWidth())
            .toOutputStream(temp_os);
        
    	temp_os.close();
    	
    	TiffImageMetadata metadata = this.readExifMetadata(largeImage);
    	OutputStream os = new FileOutputStream(output);
    	this.writeExifMetadata(metadata, temp_output, os);
    	os.close();
    	
    	temp_output.delete();
        return output;
	}


    private TiffImageMetadata readExifMetadata(File jpegFile) throws ImageReadException, IOException {
        ImageMetadata imageMetadata = Imaging.getMetadata(jpegFile);
        if (imageMetadata == null) {
            return null;
        }
        JpegImageMetadata jpegMetadata = (JpegImageMetadata)imageMetadata;
        TiffImageMetadata exif = jpegMetadata.getExif();
        if (exif == null) {
            return null;
        }
        return exif;
    }

    private OutputStream writeExifMetadata(TiffImageMetadata metadata, File jpegFile, OutputStream out) 
                                throws ImageReadException, ImageWriteException, IOException {
    	logger.info("Update Exif of " + jpegFile.getName());
        ExifRewriter writer = new ExifRewriter();
        writer.updateExifMetadataLossless(jpegFile, out, metadata.getOutputSet());
        
        return out;
    }

	
	public EncodingService getEncoding() {
		return encoding;
	}

	public void setEncoding(EncodingService encoding) {
		this.encoding = encoding;
	}

	public String getGallerieDirecotry() {
		return gallerieDirecotry;
	}

	public void setGallerieDirecotry(String gallerieDirecotry) {
		this.gallerieDirecotry = gallerieDirecotry;
	}

	public String getInputDirecotry() {
		return inputDirecotry;
	}

	public void setInputDirecotry(String inputDirecotry) {
		this.inputDirecotry = inputDirecotry;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
