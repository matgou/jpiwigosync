package info.kapable.utils.jpiwigosync.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileFinderService extends AbstractService {
	// The logger
    private static Logger logger = LoggerFactory.getLogger(FileFinderService.class);
    
    // Initial Directory
	private String initDirectory;

	public FileFinderService(String initDirectory) {
		this.initDirectory = initDirectory;
	}
	

	public void search(Consumer<String> callback) {
		this.searchAllFile(this.initDirectory, callback);
	}

	public void searchAllDirectory(String initDirectory, Consumer<String> callback) {
		try (Stream<Path> walk = Files.walk(Paths.get(initDirectory))) {

			List<String> result = walk.filter(Files::isDirectory)
					.map(x -> x.toString()).collect(Collectors.toList());
			
			result.forEach(callback);

		} catch (IOException e) {
			logger.error("Probleme while research directory",e);
		}
	}
	
	public void searchAllFile(String directory, Consumer<String> callback) {
		
		try (Stream<Path> walk = Files.walk(Paths.get(initDirectory))) {

			List<String> result = walk.filter(Files::isRegularFile)
//					.filter(p -> !p.getFileName().toString().contains(ignore_file))
//					.filter(p -> !p.toAbsolutePath().toString().contains(ignore_dir))
					.map(x -> x.toString()).collect(Collectors.toList());
			logger.info("Nb file : " + result.size());
			result.forEach(callback);
			
		} catch (IOException e) {
			logger.error("Probleme while research files",e);
		}
	}
}
