package info.kapable.utils.jpiwigosync.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.kapable.utils.jpiwigosync.model.FileMetadata;

public class FileMetadataRepositoryService {
	// The logger
    private static Logger logger = LoggerFactory.getLogger(FileMetadataRepositoryService.class);

    // reference to database for search if filemetadata exist
    private DataSource dataSource;

	Connection connection ;
	
	private void init() throws SQLException {
		this.connection = dataSource.getConnection();
	}

	public FileMetadata select(String filename, Long lastModified) throws SQLException 
	{
		if(this.connection == null) {
			this.init();
		}
		
		// Search in database the file
		String sql = "SELECT lastModified, file_hash, swift_filename from swift_file where real_filename=\"" + filename + "\"";
		logger.debug(sql);
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
	
		// if it's a new file
		FileMetadata mt=new FileMetadata(filename, "0", lastModified, false);
		
		// if the file is find
		while (resultSet.next()) {
			Long lastModifiedDB = resultSet.getLong("lastModified");
			// if it's the same between database and filesystem
			if(lastModifiedDB.equals(lastModified)) {
				logger.debug("filename exist in db and is OK");
				return null;
			}
			
			// if it's different
			mt=new FileMetadata(filename, "0", lastModified, true);
			mt.setMd5(resultSet.getString("file_hash"));
			mt.setSwiftFilename(resultSet.getString("swift_filename"));
		}
		return mt;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
