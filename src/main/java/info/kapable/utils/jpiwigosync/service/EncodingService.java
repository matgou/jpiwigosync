package info.kapable.utils.jpiwigosync.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.Normalizer;

public class EncodingService extends AbstractService {

	public String normalize(String src) {
		return Normalizer
			.normalize(src, Normalizer.Form.NFD)
			.replaceAll("[^\\p{ASCII}]", "")
			.replaceAll("'", "-")
			.replaceAll("&", "-")
			.replaceAll(" ", "-")
			.replaceAll(",", "-");
	}

	public String toUrl(String src) {
		try {
			return URLEncoder.encode(src, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return src;
		}
	}

}
