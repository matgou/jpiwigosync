package info.kapable.utils.jpiwigosync.service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openstack4j.api.OSClient;
import org.openstack4j.core.transport.Config;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.common.Payload;
import org.openstack4j.model.common.Payloads;
import org.openstack4j.model.storage.object.options.ObjectPutOptions;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import info.kapable.utils.jpiwigosync.exception.JPwigoSwiftException;
import info.kapable.utils.jpiwigosync.model.FileMetadata;

/**
 * 
 * os_auth_endpoint=https://auth.cloud.ovh.net/v2.0/
os_region=
os_username=
os_password=
os_tenant_name=
 * @author matgou
 *
 */


public class OpenstackSwiftService {
	// The logger
    private static Logger logger = LoggerFactory.getLogger(OpenstackSwiftService.class);
	@SuppressWarnings("rawtypes")
	private OSClient os;
	private String osAuthEndpoint;
	private String osRegion;
	private String osUsername;
	private String osPassword;
	private String osTenantName;
	private String containerName;
	private int connectionTimeout=600000;
	private int socketTimeout=600000;
	private EncodingService encoding;
	
	/*
	 * Connect if not
	 */
	private void connect() {        
		Config config = Config.newConfig().withConnectionTimeout(this.connectionTimeout).withReadTimeout(this.socketTimeout).withMaxConnections(1);

		this.os = OSFactory.builderV3()
				.withConfig(config)
				.endpoint(osAuthEndpoint)
				.credentials(osUsername, osPassword, Identifier.byId("default"))
				.scopeToProject(Identifier.byName(this.osTenantName), Identifier.byId("default"))
				.authenticate();
		
		this.os.useRegion(osRegion);

		logger.info("OpenStack connection");
	}

	/**
	 * Update existing photo is objectStorage
	 * @param filename
	 * @param f
	 * @throws JPwigoSwiftException 
	 */
	public synchronized void update(File f, FileMetadata fmeta) throws JPwigoSwiftException {
		if(os == null) {
			this.connect();
		}
		String filename = fmeta.getSwiftFilename();
		
		// delete old object
		ActionResponse etag = os.objectStorage().objects().delete(this.containerName, filename);
		logger.info("Delete Tx:" + etag.toString());
		if(!etag.isSuccess() && etag.getCode() != 404) {
			logger.error(etag.toString());
			throw new JPwigoSwiftException("Error while deleting swift : " + filename);
		}
		
		// Create new object
		this.create(f, fmeta);
	}

	/** 
	 * Create a new file in swift storage
	 * @param f
	 * @param fmeta
	 * @throws JPwigoSwiftException
	 */
	public synchronized void create(File f, FileMetadata fmeta) throws JPwigoSwiftException {
		if(os == null) {
			this.connect();
		}
		
		String filename = fmeta.getSwiftFilename();
		Map<String, String> md = new HashMap<String,String>();
		logger.info("Start create tx : " + filename);
		md.put("X-Object-Meta-Orealname", encoding.toUrl(fmeta.getFilename()));
		try {
			Payload<File> payload = Payloads.create(f);
			String etag = os.objectStorage().objects().put(containerName, filename, 
				payload, 
                ObjectPutOptions.create()
                   .path(filename)
                   .metadata(md)
                   
                );
			logger.info("Create Tx:" + etag);
		} catch(Exception e) {
			logger.error("Exception durring post : " , e);
			throw new JPwigoSwiftException("Exception durring post : " + e.getMessage());
		}
	}

	public String getOsAuthEndpoint() {
		return osAuthEndpoint;
	}

	public void setOsAuthEndpoint(String osAuthEndpoint) {
		this.osAuthEndpoint = osAuthEndpoint;
	}

	public String getOsRegion() {
		return osRegion;
	}

	public void setOsRegion(String osRegion) {
		this.osRegion = osRegion;
	}

	public String getOsUsername() {
		return osUsername;
	}

	public void setOsUsername(String osUsername) {
		this.osUsername = osUsername;
	}

	public String getOsPassword() {
		return osPassword;
	}

	public void setOsPassword(String osPassword) {
		this.osPassword = osPassword;
	}

	public String getOsTenantName() {
		return osTenantName;
	}

	public void setOsTenantName(String osTenantName) {
		this.osTenantName = osTenantName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public EncodingService getEncoding() {
		return encoding;
	}

	public void setEncoding(EncodingService encoding) {
		this.encoding = encoding;
	}
}
