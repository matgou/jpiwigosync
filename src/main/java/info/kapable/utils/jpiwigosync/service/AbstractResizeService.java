package info.kapable.utils.jpiwigosync.service;

import java.io.File;
import java.io.IOException;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;

public abstract class AbstractResizeService extends AbstractService {
	public abstract File buildResizeImage(File largeImage) throws IOException, ImageReadException, ImageWriteException;
}
