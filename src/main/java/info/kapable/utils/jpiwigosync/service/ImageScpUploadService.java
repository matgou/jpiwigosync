package info.kapable.utils.jpiwigosync.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import info.kapable.utils.jpiwigosync.exception.JPwigoSSHException;

public class ImageScpUploadService extends AbstractService {
	// The logger
	private static Logger logger = LoggerFactory.getLogger(ImageScpUploadService.class);

	private int port = 22;

	private Session session;
	private Channel channel;
	private ChannelSftp channelSftp;

	private EncodingService encoding;
	private String photoDirName;
	private String galleriesDirectory;
	private String host;
	private String user;
	private String keyFilePath;

	/**
	 * Construct the service to upload photo
	 * 
	 * @param galleriesDirectory
	 * @param user
	 * @param key
	 * @throws Exception
	 */
	public void connect() throws JPwigoSSHException {
		this.session = this.createSession(user, host, this.port, keyFilePath, null);
		try {
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
		} catch (JSchException e) {
			logger.error("Erreur lors de la connexion ssh vers " + host, e);
			throw new JPwigoSSHException("Erreur lors de la connexion ssh vers " + host);
		}
	}

	/**
	 * Upload a file to galleries repository
	 * 
	 * @param resizedImageFile
	 * @throws JPwigoSSHException
	 */
	public synchronized void upload(File resizedImageFile) throws JPwigoSSHException {
		if (this.session == null) {
			this.connect();
		}

		logger.info("Upload resized image file : " + resizedImageFile.getName());
		String unFormatedPath = resizedImageFile.getParentFile().getPath().substring(this.photoDirName.length());
		String formatedPath = encoding.normalize(unFormatedPath);

		try {
			logger.info("ssh mkdir : " + this.galleriesDirectory + "/" + formatedPath);
			String dirCursor = this.galleriesDirectory;
			String[] arrayDir = formatedPath.split("/");
			logger.info("size:" + Array.getLength(arrayDir));
			for (int i = 0; i < Array.getLength(arrayDir); i++) {
				dirCursor = dirCursor + "/" + arrayDir[i];
				logger.info("ssh mkdir : " + dirCursor);
				try {
					channelSftp.mkdir(dirCursor);
				} catch (SftpException e) {
					// directory exist
					logger.debug("SSH error: ", e);
				}
			}

			logger.info("ssh cd : " + this.galleriesDirectory + "/" + formatedPath);
			channelSftp.cd(this.galleriesDirectory + "/" + formatedPath);
			logger.info(
					"ssh upload : " + this.galleriesDirectory + "/" + formatedPath + "/" + resizedImageFile.getName());
			channelSftp.put(new FileInputStream(resizedImageFile), resizedImageFile.getName());
		} catch (FileNotFoundException | SftpException e) {
			logger.error("Error while uploading via ssh file", e);
			throw new JPwigoSSHException("Error while uploading via ssh file");
		}

	}

	private Session createSession(String user, String host, int port, String keyFilePath, String keyPassword)
			throws JPwigoSSHException {
		Session session = null;
		try {
			JSch jsch = new JSch();

			if (keyFilePath != null) {
				if (keyPassword != null) {
					jsch.addIdentity(keyFilePath, keyPassword);
				} else {
					jsch.addIdentity(keyFilePath);
				}
			}

			Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");

			session = jsch.getSession(user, host, port);
			session.setConfig(config);
			session.connect();
			logger.info("SSH connection => OK");
		} catch (JSchException e) {
			logger.error("Erreur lors de la connexion ssh vers " + host, e);
			throw new JPwigoSSHException("erreur lors de la connexion");
		}

		return session;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public EncodingService getEncoding() {
		return encoding;
	}

	public void setEncoding(EncodingService encoding) {
		this.encoding = encoding;
	}

	public String getPhotoDirName() {
		return photoDirName;
	}

	public void setPhotoDirName(String photoDirName) {
		this.photoDirName = photoDirName;
	}

	public String getGalleriesDirectory() {
		return galleriesDirectory;
	}

	public void setGalleriesDirectory(String galleriesDirectory) {
		this.galleriesDirectory = galleriesDirectory;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getKeyFilePath() {
		return keyFilePath;
	}

	public void setKeyFilePath(String keyFilePath) {
		this.keyFilePath = keyFilePath;
	}
}
