package info.kapable.utils.jpiwigosync.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import info.kapable.utils.jpiwigosync.model.PiwigoCategorieList;
import info.kapable.utils.jpiwigosync.model.PiwigoCategories;
import info.kapable.utils.jpiwigosync.model.PiwigoImage;
import info.kapable.utils.jpiwigosync.model.PiwigoStatus;


public class PiwigoApiService {

	private static final String URL_LST_IMAGE = "/ws.php?format=rest&method=pwg.categories.getImages&cat_id=";
	private static final String URL_PERMISSION_ADD = "/ws.php?format=rest";
	private static final String URL_STATUS = "/ws.php?format=rest&method=pwg.session.getStatus";
	private static final String URL_INFO_CATEGORIE = "/ws.php?format=rest&method=pwg.categories.getList&cat_id=";
	private static final String URL_ADD_CATEGORIE = "ws.php?format=rest&method=pwg.categories.add&visible=true&status=private&commentable=false&";
	private static final String URL_LOGIN = "/ws.php?format=rest&method=pwg.session.login";
	private static final String URL_LOGOUT = "/ws.php?format=rest&method=pwg.session.logout";
	
	private static final String URL_ADD_IMAGE_CATEGORIE = "/ws.php?format=rest";
	
	// The logger
    private static Logger logger = LoggerFactory.getLogger(PiwigoApiService.class);

	private String baseUrl;
	private String username;
	private String password;
	private String userId;
	
	private HttpClient client;
	private BasicCookieStore cookieStore;
	private HttpClientContext context;
	
	private XmlMapper xmlMapper;
	private PiwigoStatus status;
	
	private void init() throws Exception{

	    this.xmlMapper = new XmlMapper();
		this.cookieStore = new BasicCookieStore();
		this.client = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
		this.context = HttpClientContext.create();
		
		this.login();
	}
	
	private void logout() throws ClientProtocolException, IOException, InterruptedException {
		final HttpGet getRequest = new HttpGet(baseUrl + URL_LOGOUT);
		HttpResponse response = client.execute(getRequest, context);
		
		logger.debug("Connexion => " + response.getStatusLine());

	    InputStream is = response.getEntity().getContent();
	    InputStreamReader reader = new InputStreamReader(is);
	    java.io.BufferedReader buff = new java.io.BufferedReader(reader);
	    
	    buff.lines().forEach(logger::debug);;
	    EntityUtils.consume(response.getEntity());
	    
	    this.cookieStore.clear();
	}
	
	private void login() throws Exception {
		final HttpPost request = new HttpPost(baseUrl + URL_LOGIN);
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("username", this.getUsername()));
	    params.add(new BasicNameValuePair("password", this.getPassword()));
	    /*params.add(new BasicNameValuePair("redirect", ""));
	    params.add(new BasicNameValuePair("login", "Valider"));*/
	    request.setEntity(new UrlEncodedFormEntity(params));
	    
	    try {
			HttpResponse response = client.execute(request, context);
			logger.debug("Nombre de cookies:" + this.cookieStore.getCookies().size());
			
			logger.debug("Connexion => " + response.getStatusLine());

		    InputStream is = response.getEntity().getContent();
		    InputStreamReader reader = new InputStreamReader(is);
		    java.io.BufferedReader buff = new java.io.BufferedReader(reader);
		    
		    buff.lines().forEach(logger::debug);;
		    EntityUtils.consume(response.getEntity());
		    
			// get pwg_token
			String statusUrl = baseUrl + URL_STATUS;
			final HttpGet statusRequest = new HttpGet(statusUrl);
		    HttpResponse statusResult = client.execute(statusRequest, context);
		    this.status = (PiwigoStatus) xmlMapper.readValue(statusResult.getEntity().getContent(), PiwigoStatus.class);
		    EntityUtils.consume(statusResult.getEntity());
		    
		} catch (ClientProtocolException e) {
			logger.error("Error while calling login", e);
			throw new Exception(e.getMessage());
		} catch (IOException e) {
			logger.error("Error while calling login", e);
			throw new Exception(e.getMessage());
		}
	}
	
	public void addPermission(int categorieId) throws Exception {
		// add permission to user 
		String permUrl = baseUrl + URL_PERMISSION_ADD + "&pwg_token=" + status.pwgToken;
		
		final HttpPost permRequest = new HttpPost(permUrl);
		HttpEntity entity = new StringEntity("pwg_token=" + status.pwgToken + "&method=pwg.permissions.add&cat_id=" + categorieId + "&user_id=5&recursive=true");
		//logger.debug("method=pwg.permissions.add&cat_id=" + categorie.getId() + "&user_id=" + this.userId + "&recursive=true");
		permRequest.setEntity(entity);
		permRequest.setHeader("Content-type", "application/x-www-form-urlencoded");
	    HttpResponse permResult = client.execute(permRequest, context);
	    
	    logger.debug("Set permission : " + permResult.toString());
	    EntityUtils.consume(permResult.getEntity());
	    
	    this.logout();
	    this.cookieStore.clear();
	    this.login();
	}
    

	public int createCategorie(String name, int parentId) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		String url = baseUrl + URL_ADD_CATEGORIE + "name=" + URLEncoder.encode(name, "UTF-8") + "&parent=" + parentId;

		logger.debug("GET " + url);
	    final HttpGet request = new HttpGet(url);
	    HttpResponse result = client.execute(request, context);
	    logger.debug("new Categorie : " + result.getEntity().toString());
	    PiwigoCategories newCategorie = (PiwigoCategories) xmlMapper.readValue(result.getEntity().getContent(), PiwigoCategories.class);
	    
		return newCategorie.getId();
	}

	public void addImageToCategorie(PiwigoImage i, int virtualCategorieId) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		String url = baseUrl + URL_ADD_IMAGE_CATEGORIE;

		logger.debug("POST " + url);
		String body = "pwg_token=" + status.pwgToken + "&method=pwg.images.setInfo&image_id=" + i.getId() + "&categories=" + virtualCategorieId;
		logger.debug(body);
		HttpEntity entity = new StringEntity(body);
		
		final HttpPost request = new HttpPost(url);
		request.setEntity(entity);
		request.setHeader("Content-type", "application/x-www-form-urlencoded");
	    HttpResponse result = client.execute(request, context);

	    logger.debug("Set permission : " + result.toString());
	    EntityUtils.consume(result.getEntity());
	}

	public PiwigoCategories getCategorie(int id) throws Exception {
		if(this.client == null) {
			this.init();
		}

	    this.addPermission(id);
	    
		// TODO Auto-generated method stub
		String url = baseUrl + URL_INFO_CATEGORIE + id;

		logger.debug("GET " + url);
	    final HttpGet request = new HttpGet(url);
	    HttpResponse result = client.execute(request, context);
	    logger.debug("Categorie : " + result.getEntity().toString());
	    PiwigoCategorieList categories = (PiwigoCategorieList) xmlMapper.readValue(result.getEntity().getContent(), PiwigoCategorieList.class);
	    logger.debug("Categorie : " + categories.toString());
	    
	    if(categories.getCategories().length > 0) {
	    	return categories.getCategories()[0];
	    }
	    return null;
	}
	
	public PiwigoImage[] listImage(PiwigoCategories categorie) throws Exception {
		if(this.client == null) {
			this.init();
		}

	    this.addPermission(categorie.getId());
		
		// Find image of physical categories
		String url=baseUrl + URL_LST_IMAGE + categorie.getId();
		logger.debug("GET " + url);
	    final HttpGet request = new HttpGet(url);
	    HttpResponse result = client.execute(request, context);

	    InputStream is = result.getEntity().getContent();
	    InputStreamReader reader = new InputStreamReader(is);
	    java.io.BufferedReader buff = new java.io.BufferedReader(reader);
	    
	    String strCurrentLine;
	    String objectXML = "";
		while ((strCurrentLine = buff.readLine()) != null) {
	        logger.debug(strCurrentLine);
	        objectXML = objectXML + strCurrentLine;
	    }

	    PiwigoCategories imageList = (PiwigoCategories) xmlMapper.readValue(objectXML, PiwigoCategories.class);
	    
	    if(imageList.getImages() != null) {
	    	logger.debug(" => NOMBRE d'image : " + imageList.getImages().length);
	    }
	    return imageList.getImages();
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
