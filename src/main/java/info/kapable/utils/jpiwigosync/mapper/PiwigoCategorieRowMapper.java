package info.kapable.utils.jpiwigosync.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import info.kapable.utils.jpiwigosync.model.PiwigoCategories;

public class PiwigoCategorieRowMapper implements RowMapper<PiwigoCategories>{

	@Override
	public PiwigoCategories mapRow(ResultSet rs, int rowNum) throws SQLException {
		PiwigoCategories pc = new PiwigoCategories();
		pc.setId(rs.getInt("id"));
		pc.setName(rs.getString("name"));
		
		return pc;
	}

}
