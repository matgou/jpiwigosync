package info.kapable.utils.jpiwigosync.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import info.kapable.utils.jpiwigosync.model.FileMetadata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;

public class FileFinderReader implements ItemReader<FileMetadata> {
	// The logger
	private static Logger logger = LoggerFactory.getLogger(FileFinderReader.class);

	// Directory input string
	private String inputDirecotry;

	// variables
	List<String> result;
	int index = 0;

	/**
	 * Init function (perform search) and store restult in list
	 */
	private void init() {

		try (Stream<Path> walk = Files.walk(Paths.get(this.inputDirecotry))) {
			result = walk.filter(Files::isRegularFile).filter(f -> authorized_ext(f.getFileName().toString()))
					.map(x -> x.toString()).collect(Collectors.toList());
		} catch (IOException e) {
			logger.error("Probleme while research files", e);
		}
	}

	private boolean authorized_ext(String filename) {
		if (filename.toLowerCase().endsWith("jpg")) {
			return true;
		}/*
		if (filename.toLowerCase().endsWith("mov")) {
			return true;
		}*/

		return false;
	}

	@Override
	public synchronized FileMetadata read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		if (result == null) {
			this.init();
			if (result == null) {
				return null;
			}
		}

		if (index >= result.size()) {
			return null;
		}

		index++;

		String res = result.get(index - 1).substring(this.inputDirecotry.length());
		return new FileMetadata(res);
	}

	public String getInputDirecotry() {
		return inputDirecotry;
	}

	public void setInputDirecotry(String inputDirecotry) {
		this.inputDirecotry = inputDirecotry;
	}
}
