package info.kapable.utils.jpiwigosync.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.sql.DataSource;

import info.kapable.utils.jpiwigosync.model.FileMetadata;

/**
 * Write filemetadata in database
 * @author matgou
 *
 */
public class DatabaseFileMetadataWriter implements ItemWriter<FileMetadata> {

	// The logger
    private static Logger logger = LoggerFactory.getLogger(DatabaseFileMetadataWriter.class);
    
	private static final String UPDATE_SQL = "UPDATE swift_file SET "
			+ "hash = ?, swift_filename = ?, lastModified = ?, file_hash = ?, swift_container = ?"
			+ "WHERE real_filename = ?";
	private static final String INSERT_SQL = "INSERT INTO swift_file(hash, swift_filename, lastModified, file_hash, swift_container, real_filename) "
			+ "VALUES (?,?,?,?,?,?)";
	
	DataSource dataSource;
	

	
	@Override
	public void write(List<? extends FileMetadata> files) throws Exception {
		Connection connection = dataSource.getConnection();
		//connection.setAutoCommit(false);
		for(FileMetadata file: files) {
			if(file.isExistInDb()) {
		        PreparedStatement update = connection.prepareStatement(UPDATE_SQL);
		        update.setString(1, file.getHash());
		        update.setString(2, file.getSwiftFilename());
		        update.setLong(3, file.getLastModified());	
		        update.setString(4, file.getMd5());
		        update.setString(5, "photos_hd");
		        update.setString(6, file.getFilename());
	            logger.info(update.toString());
	            
	            update.executeUpdate();
			} else {
		        PreparedStatement insert = connection.prepareStatement(INSERT_SQL);
		        insert.setString(1, file.getHash());
		        insert.setString(2, file.getSwiftFilename());
		        insert.setLong(3, file.getLastModified());
		        insert.setString(4, file.getMd5());
		        insert.setString(5, "photos_hd");
		        insert.setString(6, file.getFilename());
	            logger.info(insert.toString());
	            
		        insert.executeUpdate();
			}
				
		}

		connection.commit();
		connection.close();
	}



	public DataSource getDataSource() {
		return dataSource;
	}



	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
