package info.kapable.utils.jpiwigosync.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.sql.DataSource;

import info.kapable.utils.jpiwigosync.model.PiwigoCategories;

/**
 * Write filemetadata in database
 * @author matgou
 *
 */
public class DatabaseCategorieWriter implements ItemWriter<PiwigoCategories> {

	// The logger
    private static Logger logger = LoggerFactory.getLogger(DatabaseCategorieWriter.class);
    
	// private static final String UPDATE_SQL = "";
	private static final String INSERT_SQL = "INSERT INTO piwigo_virtualized_categories(id_categorie_physical, id_categorie_virtual, date_virtualization) "
			+ "VALUES (?,?,NOW())";
	
	DataSource dataSource;
	

	
	@Override
	public void write(List<? extends PiwigoCategories> categories) throws Exception {
		Connection connection = dataSource.getConnection();
		//connection.setAutoCommit(false);
		for(PiwigoCategories categorie: categories) {
			    PreparedStatement insert = connection.prepareStatement(INSERT_SQL);
		        insert.setInt(1, categorie.getId());
		        insert.setInt(2, categorie.getVirtualId());
	            logger.info(insert.toString());
	            
	            insert.executeUpdate();
		}

		connection.commit();
		connection.close();
	}



	public DataSource getDataSource() {
		return dataSource;
	}



	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
