package info.kapable.utils.jpiwigosync.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name="rsp")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PiwigoStatus {
// <username>batch</username><status>webmaster</status><theme>bootstrapdefault</theme><language>fr_FR</language><pwg_token>59507182b956a70862f24822cfeefc10</pwg_token><charset>utf-8</charset><current_datetime>2019-05-30 15:49:55</current_datetime><version>2.7.4</version><upload_file_types>jpg,jpeg,png,gif</upload_file_types>

	@JsonProperty("username")
	String username;

	@JsonProperty("pwg_token")
	public String pwgToken;
	
	

}
