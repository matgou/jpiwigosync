package info.kapable.utils.jpiwigosync.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement(name="rsp")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PiwigoCategorieList {
	PiwigoCategories[] categories;

	public PiwigoCategories[] getCategories() {
		return categories;
	}

	public void setCategories(PiwigoCategories[] categories) {
		this.categories = categories;
	}
	
	@Override
	public String toString () {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
