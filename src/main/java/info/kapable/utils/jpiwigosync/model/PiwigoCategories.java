package info.kapable.utils.jpiwigosync.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name="category")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PiwigoCategories {
	int id;
	
	int virtualId;

	@JsonProperty("id_uppercat")
	int parentId;
	int virtualParentId;
	
	String name;
	
	PiwigoImage[] images;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PiwigoImage[] getImages() {
		return images;
	}
	public void setImages(PiwigoImage[] images) {
		this.images = images;
	}
	public int getVirtualId() {
		return virtualId;
	}
	public void setVirtualId(int virtualId) {
		this.virtualId = virtualId;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	
	public int getVirtualParentId() {
		return virtualParentId;
	}
	public void setVirtualParentId(int virtualParentId) {
		this.virtualParentId = virtualParentId;
	}
	@Override
	public String toString () {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
