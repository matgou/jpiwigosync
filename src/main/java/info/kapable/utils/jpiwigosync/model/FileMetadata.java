package info.kapable.utils.jpiwigosync.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="fileMetadata")
public class FileMetadata implements Comparable<FileMetadata> {
	private String md5 = "0";
	private String filename;
	private String swiftFilename;
	private String hash;
	private long lastModified;
	private boolean existInDb;
	private String resizedFilePath;
	private boolean isUploaded = false;
	
	public FileMetadata() { }
	
	public FileMetadata(String real_filename, String md5, long lastModified, boolean existInDb) {
		this.setFilename(real_filename);
		this.setMd5(md5);
		this.setLastModified(lastModified);
		this.setExistInDb(existInDb);
	}

	public FileMetadata(String filename) {
		this.filename = filename;
	}

	@Override
	public int compareTo(FileMetadata o) {
		FileMetadata fmeta = (FileMetadata) o;
		if(this.getLastModified() == fmeta.getLastModified()) {
			return this.getFilename().compareTo(fmeta.getFilename());
		}
		return 1;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public String getSwiftFilename() {
		return swiftFilename;
	}

	public void setSwiftFilename(String swiftFilename) {
		this.swiftFilename = swiftFilename;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public boolean isExistInDb() {
		return existInDb;
	}

	public void setExistInDb(boolean existInDb) {
		this.existInDb = existInDb;
	}

	public String getResizedFilePath() {
		return resizedFilePath;
	}

	public void setResizedFilePath(String resizedFilePath) {
		this.resizedFilePath = resizedFilePath;
	}

	public boolean isUploaded() {
		return isUploaded;
	}

	public void setUploaded(boolean isUploaded) {
		this.isUploaded = isUploaded;
	}

	public String getFileExtention() {
		return this.getFilename().toLowerCase().substring(this.getFilename().lastIndexOf(".") + 1);
	}
	
}
