#!/bin/bash

CLASSPATH=$( pwd )/target/jpiwigosync-1.0.0.jar

for fic in $( pwd )/target/lib/*jar
do
	export CLASSPATH=$fic:$CLASSPATH
done

echo $CLASSPATH
java -Xmx1024m -Xms1024m -Dconfig.path=$(pwd)/jpiwigosync.properties info.kapable.utils.jpiwigosync.JPiwigoSync
