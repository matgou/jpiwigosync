#!/bin/bash

cd $( dirname $0 )
dir_=$(pwd)
echo $dir_
CLASSPATH=${dir_}:${dir_}/target/jpiwigosync-1.0.0.jar

for fic in ${dir_}/target/lib/*jar
do
	export CLASSPATH=$fic:$CLASSPATH
done

echo $CLASSPATH
cd /mnt/photos
ls ${dir_}/jpiwigosync.properties
java -Xmx1024m -Xms1024m -Dconfig.path=${dir_}/jpiwigosync.properties info.kapable.utils.jpiwigosync.JPiwigoSync job-virtualcat.xml
