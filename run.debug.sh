#!/bin/bash

echo $( dirname $0 )
cd $( dirname $0 )
PWD=$(pwd)
CLASSPATH=${PWD}:${PWD}/target/jpiwigosync-1.0.0.jar

for fic in ${PWD}/target/lib/*jar
do
	export CLASSPATH=$fic:$CLASSPATH
done

echo $CLASSPATH
#cd /mnt/photos
java -Xmx1024m -Xms1024m -Dconfig.path=$(pwd)/jpiwigosync.properties info.kapable.utils.jpiwigosync.JPiwigoSync job-jpiwigosync.xml
